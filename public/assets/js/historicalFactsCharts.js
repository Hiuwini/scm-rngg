'use strict';

var _extends = Object.assign || function(target) {
    for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];
        for (var key in source) {
            if (Object.prototype.hasOwnProperty.call(source, key)) {
                target[key] = source[key];
            }
        }
    }
    return target;
};

// Variables de Genero
var male = $('#g_male').val();
var female = $('#g_female').val();

// Variables de Rangos de Edades
var range_age_1 = $('#g_range_age_1').val();
var range_age_2 = $('#g_range_age_2').val();
var range_age_3 = $('#g_range_age_3').val();
var range_age_4 = $('#g_range_age_4').val();
var range_age_5 = $('#g_range_age_5').val();


// Variables de Ejes Estrategicos
var eje_1 = $('#g_eje_1').val();
var eje_2 = $('#g_eje_2').val();
var eje_3 = $('#g_eje_3').val();


$(document).ready(function() {

            /* Grafica de género */
            var echartElemPie = document.getElementById('echartPieGenero');
            if (echartElemPie) {
                var echartPie = echarts.init(echartElemPie);
                echartPie.setOption({
                    color: ['green', 'blue'],
                    tooltip: {
                        show: true,
                        backgroundColor: 'rgba(0, 0, 0, .8)'
                    },
                    series: [{
                        name: 'Genero',
                        type: 'pie',
                        radius: '50%',
                        center: ['50%', '50%'],
                        data: [{
                                value: male,
                                name: 'Masculino'
                            },
                            {
                                value: female,
                                name: 'Femenino'
                            }
                        ],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }]
                });
            }
                /* Grafica de rangos de edades */
                var echartElemPie = document.getElementById('echartPieEdades');
                if (echartElemPie) {
                    var echartPie = echarts.init(echartElemPie);
                    echartPie.setOption({
                        tooltip: {
                            show: true,
                            backgroundColor: 'rgba(0, 0, 0, .8)'
                        },
                        series: [{
                            name: 'Genero',
                            type: 'pie',
                            radius: '50%',
                            center: ['50%', '50%'],
                            data: [{
                                    value: range_age_1,
                                    name: 'Menor de 18 años'
                                },
                                {
                                    value: range_age_2,
                                    name: '18 - 30 años'
                                },
                                {
                                    value: range_age_3,
                                    name: '31 - 49 años'
                                },
                                {
                                    value: range_age_4,
                                    name: '50 - 60 años'
                                },
                                {
                                    value: range_age_5,
                                    name: 'Mayor de 60 años'
                                }
                            ],
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }]
                    });
                }

                    /* Grafica de ejes estrategicos */
                    var echartElemPie = document.getElementById('echartPieEjes');
                    if (echartElemPie) {
                        var echartPie = echarts.init(echartElemPie);
                        echartPie.setOption({
                            tooltip: {
                                show: true,
                                backgroundColor: 'rgba(0, 0, 0, .8)'
                            },
                            series: [{
                                name: 'Genero',
                                type: 'pie',
                                radius: '50%',
                                center: ['50%', '50%'],
                                data: [{
                                        value: eje_1,
                                        name: 'Desarrollo Productivo '
                                    },
                                    {
                                        value: eje_2,
                                        name: 'Clima de negocios'
                                    },
                                    {
                                        value: eje_3,
                                        name: 'Desarrollo Social'
                                    }
                                ],
                                itemStyle: {
                                    emphasis: {
                                        shadowBlur: 10,
                                        shadowOffsetX: 0,
                                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                                    }
                                }
                            }]
                        });
                        
                        $(window).on('resize', function() {
                            setTimeout(function() {
                                echartPie.resize();
                            }, 2000);
                        });
                    }
                });