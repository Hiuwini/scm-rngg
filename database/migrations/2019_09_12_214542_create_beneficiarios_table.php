<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeneficiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombrebeneficiario');
            $table->string('apellidobeneficiario');
            $table->string('genero');
            $table->boolean('estado')->nullable(); //No se esta utilizando
            $table->string('rangoedad');
            $table->string('nombreubicacion');
            $table->string('dpicui')->nullable();
            $table->string('telefono')->nullable();
            $table->string('emailbeneficiario')->nullable();
            $table->string('tipobeneficiario')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiarios');
    }
}
