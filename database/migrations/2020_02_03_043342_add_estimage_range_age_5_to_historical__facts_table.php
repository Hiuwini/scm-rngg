<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstimageRangeAge5ToHistoricalFactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historical__facts', function (Blueprint $table) {
            //
            $table->integer('estimate_range_age_5')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historical__facts', function (Blueprint $table) {
            //
            $table->dropColumn('estimate_range_age_5');
        });
    }
}
