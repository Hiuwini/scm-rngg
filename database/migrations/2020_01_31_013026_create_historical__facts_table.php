<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricalFactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical__facts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('qt_beneficiaries')->nullable();
            $table->date('date_of_verification')->nullable();
            $table->integer('estimate_male')->nullable();
            $table->integer('estimate_female')->nullable();
            $table->integer('estimate_range_age_1')->nullable();
            $table->integer('estimate_range_age_2')->nullable();
            $table->integer('estimate_range_age_3')->nullable();
            $table->integer('estimate_range_age_4')->nullable();
            $table->integer('estimate_eje_1')->nullable();
            $table->integer('estimate_eje_2')->nullable();
            $table->integer('estimate_eje_3')->nullable();
            $table->date('date_of_generation');
            
            $table->unsignedBigInteger('user_generator_id');
            $table->foreign('user_generator_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical__facts');
    }
}
