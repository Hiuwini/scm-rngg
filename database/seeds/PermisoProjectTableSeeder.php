<?php

use Illuminate\Database\Seeder;

class PermisoProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('permisos_projects')->delete();

        $permisos_projects = [
            //Gaby Cahuex
            ['id' => 1,'user_id' => 1,'project_id' => 1],
            ['id' => 2,'user_id' => 1,'project_id' => 2],
            ['id' => 3,'user_id' => 1,'project_id' => 3],
            ['id' => 4,'user_id' => 1,'project_id' => 4],
            ['id' => 5,'user_id' => 1,'project_id' => 5],
            ['id' => 6,'user_id' => 1,'project_id' => 6],
            ['id' => 7,'user_id' => 1,'project_id' => 7],
            ['id' => 8,'user_id' => 1,'project_id' => 8],
            ['id' => 9,'user_id' => 1,'project_id' => 9],
            ['id' => 10,'user_id' => 1,'project_id' => 10],
            ['id' => 11,'user_id' => 1,'project_id' => 11],
            ['id' => 12,'user_id' => 1,'project_id' => 12],
            ['id' => 13,'user_id' => 1,'project_id' => 13],
            ['id' => 14,'user_id' => 1,'project_id' => 14],
            ['id' => 15,'user_id' => 1,'project_id' => 15],

            //Ivan Dieguez
            ['id' => 16,'user_id' => 2,'project_id' => 1],
            ['id' => 17,'user_id' => 2,'project_id' => 2],
            ['id' => 18,'user_id' => 2,'project_id' => 3],
            ['id' => 19,'user_id' => 2,'project_id' => 4],
            ['id' => 20,'user_id' => 2,'project_id' => 5],
            ['id' => 21,'user_id' => 2,'project_id' => 6],
            ['id' => 22,'user_id' => 2,'project_id' => 7],
            ['id' => 23,'user_id' => 2,'project_id' => 8],
            ['id' => 24,'user_id' => 2,'project_id' => 9],
            ['id' => 25,'user_id' => 2,'project_id' => 10],
            ['id' => 26,'user_id' => 2,'project_id' => 11],
            ['id' => 27,'user_id' => 2,'project_id' => 12],
            ['id' => 28,'user_id' => 2,'project_id' => 13],
            ['id' => 29,'user_id' => 2,'project_id' => 14],
            ['id' => 30,'user_id' => 2,'project_id' => 15],

            //Alejandro Arango
            ['id' => 31,'user_id' => 3,'project_id' => 1],
            ['id' => 32,'user_id' => 3,'project_id' => 2],
            ['id' => 33,'user_id' => 3,'project_id' => 3],
            ['id' => 34,'user_id' => 3,'project_id' => 4],
            ['id' => 35,'user_id' => 3,'project_id' => 5],
            ['id' => 36,'user_id' => 3,'project_id' => 6],
            ['id' => 37,'user_id' => 3,'project_id' => 7],
            ['id' => 38,'user_id' => 3,'project_id' => 8],
            ['id' => 39,'user_id' => 3,'project_id' => 9],
            ['id' => 40,'user_id' => 3,'project_id' => 10],
            ['id' => 41,'user_id' => 3,'project_id' => 11],
            ['id' => 42,'user_id' => 3,'project_id' => 12],
            ['id' => 43,'user_id' => 3,'project_id' => 13],
            ['id' => 44,'user_id' => 3,'project_id' => 14],
            ['id' => 45,'user_id' => 3,'project_id' => 15],

            //Jhonny Hernandez
            ['id' => 46,'user_id' => 4,'project_id' => 2],
            ['id' => 47,'user_id' => 4,'project_id' => 3],
            ['id' => 48,'user_id' => 4,'project_id' => 4],

            //Lester Herrera
            ['id' => 49,'user_id' => 5,'project_id' => 14],

            //David Mazariegos
            ['id' => 50,'user_id' => 6,'project_id' => 5],
            ['id' => 51,'user_id' => 6,'project_id' => 6],

            //Sintia Motta
            ['id' => 52,'user_id' => 7,'project_id' => 4],
            
            //Elmar De León
            ['id' => 53,'user_id' => 8,'project_id' => 4],

             //Jose Luis Tiguila
             ['id' => 54,'user_id' => 9,'project_id' => 3],
            
        ];

        DB::table('permisos_projects')->insert($permisos_projects);
    }
}
