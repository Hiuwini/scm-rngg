<?php

use Illuminate\Database\Seeder;

class HistoricalFactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('historical__facts')->delete();

        $hf = [
            [
                'qt_beneficiaries' => 10, 
                'date_of_verification' => '2020-02-03',
                'estimate_male' => 1,
                'estimate_female' => 1,
                'estimate_range_age_1' => 1,
                'estimate_range_age_2' => 1,
                'estimate_range_age_3' => 1,
                'estimate_range_age_4' => 1,
                'estimate_range_age_5' => 1,
                'estimate_eje_1' => 1,
                'estimate_eje_2' => 1,
                'estimate_eje_3' => 1,
                'date_of_generation' => '2020-02-03',
                'user_generator_id' => 1
            ],
        ];

        DB::table('historical__facts')->insert($hf);
    }
}
