<?php

namespace App\Http\Controllers;

use App\Historical_Fact;
use App\Beneficiarios;

use Illuminate\Http\Request;

class HistoricalFactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $hf = Historical_Fact::latest()->first();
        return view('historical_facts.index')->with('hf', $hf);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $hf = new Historical_Fact;
        $hf->date_of_verification = date("Y-m-d", strtotime( $request->date_of_verification ) );
        $hf->date_of_generation = date("Y-m-d");
        $hf->qt_beneficiaries = $request->qt_beneficiaries;
        $hf->user_generator_id = auth()->user()->id;
        /* Consultar total de beneficiarios */
        $tBeneficiaries = Beneficiarios::count();

        /* Consultar totales de genero */
        $tMale = Beneficiarios::where('genero','=', 'Masculino')->count();
        $tFemale = Beneficiarios::where('genero','=', 'Femenino')->count();

        /* Consultar totales de rangos de edades */
        $tRangeAge1 = Beneficiarios::where('rangoedad','=', 'Menor de 18')->count();
        $tRangeAge2 = Beneficiarios::where('rangoedad','=', '18 - 30')->count();
        $tRangeAge3 = Beneficiarios::where('rangoedad','=', '31 - 49')->count();
        $tRangeAge4 = Beneficiarios::where('rangoedad','=', '50 - 60')->count();
        $tRangeAge5 = Beneficiarios::where('rangoedad','=', 'Mayor a 60')->count();

        /* Consultar totales de ejes estrategicos */
        //ESTO QUEDA BAJO CONSULTA A DIRECTIVOS RED NACIONAL DE GRUPOS GESTORES

        /* Aplicacion de media artimetica */
        $pMale= $tMale / $tBeneficiaries;
        $pFemale = $tFemale / $tBeneficiaries;

        $pRangeAge1 = $tRangeAge1 / $tBeneficiaries;
        $pRangeAge2 = $tRangeAge2 / $tBeneficiaries;
        $pRangeAge3 = $tRangeAge3 / $tBeneficiaries;
        $pRangeAge4 = $tRangeAge4 / $tBeneficiaries;
        $pRangeAge5 = $tRangeAge5 / $tBeneficiaries;
        
        $hf->estimate_male = intval(round($pMale * $request->qt_beneficiaries));
        $hf->estimate_female = intval(round($pFemale * $request->qt_beneficiaries));
        
        $hf->estimate_range_age_1 = intval(round($pRangeAge1 * $request->qt_beneficiaries));
        $hf->estimate_range_age_2 = intval(round($pRangeAge2 * $request->qt_beneficiaries));
        $hf->estimate_range_age_3 = intval(round($pRangeAge3 * $request->qt_beneficiaries));
        $hf->estimate_range_age_4 = intval(round($pRangeAge4 * $request->qt_beneficiaries));
        $hf->estimate_range_age_5 = intval(round($pRangeAge5 * $request->qt_beneficiaries));
        
        $hf->save();

        return redirect('/set_historical_facts'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Historical_Fact  $historical_Fact
     * @return \Illuminate\Http\Response
     */
    public function show(Historical_Fact $historical_Fact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Historical_Fact  $historical_Fact
     * @return \Illuminate\Http\Response
     */
    public function edit(Historical_Fact $historical_Fact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Historical_Fact  $historical_Fact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Historical_Fact $historical_Fact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Historical_Fact  $historical_Fact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Historical_Fact $historical_Fact)
    {
        //
    }
}
