<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Permiso;
use App\Beneficio;
use App\Participantes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class reporteController extends Controller
{

    public function generar()
    {
        $users = \DB::table('users')
        -> select(['id','firstname','lastname','username'])
            ->get();
            $view = \View::make('ReporteUsuario.index', compact('users'))->render();
            $ReporteUsuario = \App::make('dompdf.wrapper');
            $ReporteUsuario->loadHTML($view);
            return $ReporteUsuario->stream('informe'.'.pdf');
            //dd($users);
            
    }

    public function index()
    {
        
 
        $idUser=Auth::id(); //obteniendo el id del usuario 
        $idRol=Permiso::select('rol_id')->where('user_id','=',$idUser)->get();
  
            if($idRol[0]->rol_id== 1 || $idRol[0]->rol_id == 2){
      //Sending all benefits on database if user is administrator
      $projects=Project::all();        
      $beneficiarios = DB::table('beneficiarios')
            ->join('beneficios', 'beneficiarios.id', '=', 'beneficios.beneficiario_id')
            ->join('indicators', 'beneficios.indicator_id', '=', 'indicators.id')
            ->join('projects', 'indicators.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'indicators.name as participacion','projects.name as proyecto')
            ->orderBy('projects.name')
            ->get();
        $cantH=DB::table('beneficiarios')
        ->join('beneficios', 'beneficiarios.id', '=', 'beneficios.beneficiario_id')
        ->join('indicators', 'beneficios.indicator_id', '=', 'indicators.id')
        ->join('projects', 'indicators.id_proyecto', '=', 'projects.id')
        ->select('beneficiarios.*',  'indicators.name as participacion','projects.name as proyecto')
        ->where('beneficiarios.genero','=','Masculino')
        ->count(); 

        $cantM=DB::table('beneficiarios')
        ->join('beneficios', 'beneficiarios.id', '=', 'beneficios.beneficiario_id')
        ->join('indicators', 'beneficios.indicator_id', '=', 'indicators.id')
        ->join('projects', 'indicators.id_proyecto', '=', 'projects.id')
        ->select('beneficiarios.*',  'indicators.name as participacion','projects.name as proyecto')
        ->where('beneficiarios.genero','=','Femenino')
        ->count();

        $rangoUno=Beneficio::join('beneficiarios','beneficiarios.id',"=",'beneficios.beneficiario_id')
                                ->whereRaw("beneficiarios.rangoedad = 'Menor de 18' " )->count();    
        $rangoDos=Beneficio::join('beneficiarios','beneficiarios.id',"=",'beneficios.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '18 - 30'" )->count();    
        $rangoTres=Beneficio::join('beneficiarios','beneficiarios.id',"=",'beneficios.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '31 - 49' ")->count();
    
        $rangoCuatro=Beneficio::join('beneficiarios','beneficiarios.id',"=",'beneficios.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '50 - 60' ")->count();
    
        $rangoCinco=Beneficio::join('beneficiarios','beneficiarios.id',"=",'beneficios.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = 'Mayor de 60'")->count();
    
    
        $maxGenero=max($cantM,$cantH);
        $maximo=max($rangoUno,$rangoDos,$rangoTres,$rangoCuatro,$rangoCinco);
            
            
        }      
  

  
     else{
    $projects=Project::where('user_id','=',$idUser)->get();
    $cantprojects = DB::table('projects')
    ->where('user_id','=',$idUser)
    ->count();

  $beneficiarios = DB::table('beneficiarios')
            ->join('beneficios', 'beneficiarios.id', '=', 'beneficios.beneficiario_id')
            ->join('indicators', 'beneficios.indicator_id', '=', 'indicators.id')
            ->join('projects', 'indicators.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'indicators.name as participacion','projects.name as proyecto')
            ->where('projects.user_id','=',$idUser)
            ->get();    
  
            $cantH=DB::table('beneficiarios')
            ->join('beneficios', 'beneficiarios.id', '=', 'beneficios.beneficiario_id')
            ->join('indicators', 'beneficios.indicator_id', '=', 'indicators.id')
            ->join('projects', 'indicators.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'indicators.name as participacion','projects.name as proyecto')
            ->where('beneficiarios.genero','=','Masculino')
            ->count(); 
    
            $cantM=DB::table('beneficiarios')
            ->join('beneficios', 'beneficiarios.id', '=', 'beneficios.beneficiario_id')
            ->join('indicators', 'beneficios.indicator_id', '=', 'indicators.id')
            ->join('projects', 'indicators.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'indicators.name as participacion','projects.name as proyecto')
            ->where('beneficiarios.genero','=','Femenino')
            ->count();

        $rangoUno=Beneficio::join('beneficiarios','beneficiarios.id',"=",'beneficios.beneficiario_id')
                                ->whereRaw("beneficiarios.rangoedad = 'Menor de 18' " )->count();    
        $rangoDos=Beneficio::join('beneficiarios','beneficiarios.id',"=",'beneficios.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '18 - 30'" )->count();    
        $rangoTres=Beneficio::join('beneficiarios','beneficiarios.id',"=",'beneficios.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '31 - 49' ")->count();
    
        $rangoCuatro=Beneficio::join('beneficiarios','beneficiarios.id',"=",'beneficios.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '50 - 60' ")->count();
    
        $rangoCinco=Beneficio::join('beneficiarios','beneficiarios.id',"=",'beneficios.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = 'Mayor de 60'")->count();
    
    
        $maxGenero=max($cantM,$cantH);
        $maximo=max($rangoUno,$rangoDos,$rangoTres,$rangoCuatro,$rangoCinco);
    

  }
  //Retornamos la vista ya con los datos anclados
  $totalbeneficiarios=count($beneficiarios);
        return view('reportes.listado_reportes')
        ->with('beneficiarios',$beneficiarios)
        ->with('projects',$projects)
        ->with('cantH',$cantH)
        ->with('cantM',$cantM)
        ->with('totalbeneficiarios',$totalbeneficiarios)
        ->with('maxGenero',$maxGenero)
        ->with('rangoUno',$rangoUno)
        ->with('rangoDos',$rangoDos)
        ->with('rangoTres',$rangoTres)
        ->with('rangoCuatro',$rangoCuatro)
        ->with('rangoCinco',$rangoCinco)
        ->with('maximo',$maximo);
        
    }

    public function reportbyEvents()
    {
        
 
 $idUser=Auth::id(); //obteniendo el id del usuario 
 $idRol=Permiso::select('rol_id')->where('user_id','=',$idUser)->get();
  
  if($idRol[0]->rol_id== 1 || $idRol[0]->rol_id == 2){
      //Sending all benefits on database if user is administrator
      $projects=Project::all();        
      $beneficiarios = DB::table('beneficiarios')
            ->join('participantes', 'beneficiarios.id', '=', 'participantes.beneficiario_id')
            ->join('actividades', 'participantes.actividad_id', '=', 'actividades.id')
            ->join('projects', 'actividades.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'actividades.nombre as participacion','projects.name as proyecto')
            ->orderBy('projects.name')
            ->get();

    $cantH = DB::table('beneficiarios')
            ->join('participantes', 'beneficiarios.id', '=', 'participantes.beneficiario_id')
            ->join('actividades', 'participantes.actividad_id', '=', 'actividades.id')
            ->join('projects', 'actividades.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'actividades.nombre as participacion','projects.name as proyecto')
            ->where('beneficiarios.genero','=','Masculino')
            ->count();
    $cantM = DB::table('beneficiarios')
            ->join('participantes', 'beneficiarios.id', '=', 'participantes.beneficiario_id')
            ->join('actividades', 'participantes.actividad_id', '=', 'actividades.id')
            ->join('projects', 'actividades.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'actividades.nombre as participacion','projects.name as proyecto')
            ->where('beneficiarios.genero','=','Femenino')
            ->count();

    $rangoUno=Participantes::join('beneficiarios','beneficiarios.id',"=",'participantes.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = 'Menor de 18' " )->count();    
    $rangoDos=Participantes::join('beneficiarios','beneficiarios.id',"=",'participantes.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '18 - 30'" )->count();    
    $rangoTres=Participantes::join('beneficiarios','beneficiarios.id',"=",'participantes.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '31 - 49' ")->count();

    $rangoCuatro=Participantes::join('beneficiarios','beneficiarios.id',"=",'participantes.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '50 - 60' ")->count();

    $rangoCinco=Participantes::join('beneficiarios','beneficiarios.id',"=",'participantes.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = 'Mayor de 60'")->count();


$maxGenero=max($cantM,$cantH);
$maximo=max($rangoUno,$rangoDos,$rangoTres,$rangoCuatro,$rangoCinco);

            
  }
  else{
  $projects=Project::where('user_id','=',$idUser)->get();
  $cantprojects = DB::table('projects')
  ->where('user_id','=',$idUser)
  ->count();

  $beneficiarios = DB::table('beneficiarios')
            ->join('participantes', 'beneficiarios.id', '=', 'participantes.beneficiario_id')
            ->join('actividades', 'participantes.actividad_id', '=', 'actividades.id')
            ->join('projects', 'actividades.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'actividades.nombre as participacion','projects.name as proyecto')
            ->where('projects.user_id','=',$idUser)
            ->get();

    $cantH = DB::table('beneficiarios')
            ->join('participantes', 'beneficiarios.id', '=', 'participantes.beneficiario_id')
            ->join('actividades', 'participantes.actividad_id', '=', 'actividades.id')
            ->join('projects', 'actividades.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'actividades.nombre as participacion','projects.name as proyecto')
            ->where('projects.user_id','=',$idUser)
            ->where('beneficiarios.genero','=','Masculino')
            ->count();
    $cantM = DB::table('beneficiarios')
            ->join('participantes', 'beneficiarios.id', '=', 'participantes.beneficiario_id')
            ->join('actividades', 'participantes.actividad_id', '=', 'actividades.id')
            ->join('projects', 'actividades.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'actividades.nombre as participacion','projects.name as proyecto')
            ->where('projects.user_id','=',$idUser)
            ->where('beneficiarios.genero','=','Femenino')
            ->count();

    $rangoUno=Participantes::join('beneficiarios','beneficiarios.id',"=",'participantes.beneficiario_id')
            ->join('actividades','participantes.actividad_id',"=",'actividades.id')
            ->join('projects','actividades.id_proyecto','=','projects.id')
            ->whereRaw("beneficiarios.rangoedad = 'Menor de 18' and projects.user_id =". $idUser )->count();    
    $rangoDos=Participantes::join('beneficiarios','beneficiarios.id',"=",'participantes.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '18 - 30'" )->count();    
    $rangoTres=Participantes::join('beneficiarios','beneficiarios.id',"=",'participantes.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '31 - 49' ")->count();

    $rangoCuatro=Participantes::join('beneficiarios','beneficiarios.id',"=",'participantes.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = '50 - 60' ")->count();

    $rangoCinco=Participantes::join('beneficiarios','beneficiarios.id',"=",'participantes.beneficiario_id')
            ->whereRaw("beneficiarios.rangoedad = 'Mayor de 60'")->count();

        $maxGenero=max($cantM,$cantH);
        $maximo=max($rangoUno,$rangoDos,$rangoTres,$rangoCuatro,$rangoCinco);
  
  }
  //Retornamos la vista ya con los datos anclados
  $totalbeneficiarios=count($beneficiarios);
        return view('reportes.listado_reportes')
        ->with('beneficiarios',$beneficiarios)
        ->with('projects',$projects)
        ->with('cantH',$cantH)
        ->with('cantM',$cantM)
        ->with('totalbeneficiarios',$totalbeneficiarios)
        ->with('maxGenero',$maxGenero)
        ->with('rangoUno',$rangoUno)
        ->with('rangoDos',$rangoDos)
        ->with('rangoTres',$rangoTres)
        ->with('rangoCuatro',$rangoCuatro)
        ->with('rangoCinco',$rangoCinco)
        ->with('maximo',$maximo);

    }

    public function beneficiarioporEvento(Request $request)
    {
        
 
 $idUser=Auth::id(); //obteniendo el id del usuario 
 $idRol=Permiso::select('rol_id')->where('user_id','=',$idUser)->get();

 $idactividad=$request->idactivity;
  $idproject=$request->idproject;
  
      //Sending all benefits on database if user is administrator
      $projects=Project::all();        
      $beneficiarios = DB::table('beneficiarios')
            ->join('participantes', 'beneficiarios.id', '=', 'participantes.beneficiario_id')
            ->join('actividades', 'participantes.actividad_id', '=', 'actividades.id')
            ->join('projects', 'actividades.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'actividades.nombre as participacion','projects.name as proyecto')
            ->where('actividades.id','=',$idactividad)
            ->orderBy('projects.name')
            ->get();

            $cantH = DB::table('beneficiarios')
            ->join('participantes', 'beneficiarios.id', '=', 'participantes.beneficiario_id')
            ->join('actividades', 'participantes.actividad_id', '=', 'actividades.id')
            ->join('projects', 'actividades.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'actividades.nombre as participacion','projects.name as proyecto')
            ->where('actividades.id','=',$idactividad)
            ->where('beneficiarios.genero','=','Masculino')
            ->count();

            $cantM = DB::table('beneficiarios')
            ->join('participantes', 'beneficiarios.id', '=', 'participantes.beneficiario_id')
            ->join('actividades', 'participantes.actividad_id', '=', 'actividades.id')
            ->join('projects', 'actividades.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'actividades.nombre as participacion','projects.name as proyecto')
            ->where('actividades.id','=',$idactividad)
            ->where('beneficiarios.genero','=','Femenino')
            ->count();
  
  $totalbeneficiarios=count($beneficiarios);
  //Retornamos la vista ya con los datos anclados
        return view('reportes.listado_reportes')
        ->with('beneficiarios',$beneficiarios)
        ->with('projects',$projects)
        ->with('totalbeneficiarios',$totalbeneficiarios)
        ->with('cantH',$cantH)
        ->with('cantM',$cantM);
        
    }



public function beneficiarioporIndicador(Request $request)
    {
        
 //Sending all projects on database
 $idUser=Auth::id(); //obteniendo el id del usuario
 $idRol=Permiso::select('rol_id')->where('user_id','=',$idUser)->get();  
  
  
  $idindicador=$request->idindicator;
  $idproject=$request->idproject;
  //Obteniendo los beneficiarios que esten registrado en la tabla beneficios y que tengan el indicador que selecciono el usuario
$projects=Project::All();

            $beneficiarios = DB::table('beneficiarios')
            ->join('beneficios', 'beneficiarios.id', '=', 'beneficios.beneficiario_id')
            ->join('indicators', 'beneficios.indicator_id', '=', 'indicators.id')
            ->join('projects', 'indicators.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'indicators.name as participacion','projects.name as proyecto')
            ->where('indicators.id','=', $idindicador)
            ->orderBy('projects.name')
            ->get();

            $cantH = DB::table('beneficiarios')
            ->join('beneficios', 'beneficiarios.id', '=', 'beneficios.beneficiario_id')
            ->join('indicators', 'beneficios.indicator_id', '=', 'indicators.id')
            ->join('projects', 'indicators.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'indicators.name as participacion','projects.name as proyecto')
            ->where('indicators.id','=', $idindicador)
            ->where('beneficiarios.genero','=','Masculino')            
            ->count();

            $cantM = DB::table('beneficiarios')
            ->join('beneficios', 'beneficiarios.id', '=', 'beneficios.beneficiario_id')
            ->join('indicators', 'beneficios.indicator_id', '=', 'indicators.id')
            ->join('projects', 'indicators.id_proyecto', '=', 'projects.id')
            ->select('beneficiarios.*',  'indicators.name as participacion','projects.name as proyecto')
            ->where('indicators.id','=', $idindicador)
            ->where('beneficiarios.genero','=','Femenino')            
            ->count();           

  //Retornamos la vista ya con los datos anclados
        $totalbeneficiarios=count($beneficiarios);
        
        return view('reportes.listado_reportes')->with('projects',$projects)
        ->with('indicador',$idindicador)
        ->with('beneficiarios',$beneficiarios)
        ->with('cantH',$cantH)
        ->with('cantM',$cantM)
        ->with('totalbeneficiarios',$totalbeneficiarios);
    }



}
