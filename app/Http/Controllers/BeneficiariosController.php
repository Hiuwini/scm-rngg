<?php

namespace App\Http\Controllers;

use App\Beneficiarios;
use App\Beneficio;
use App\TipoGestor;
use App\Tipobeneficiarios;
use App\Indicator;
use App\Project;
use App\Participantes;
use App\Actividades;
use Illuminate\Http\Request;
 
class BeneficiariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //$beneficiarios = Beneficiarios::all();
        $beneficiarios = Beneficiarios::orderBy('id','ASC')->paginate(10);
        return view('beneficiarios/index', compact('beneficiarios'))->with('i',(request()->input('page', 1) - 1) *10);       
           

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $tiposgestores= TipoGestor::all();     
        $tipobeneficiarios = Tipobeneficiarios::all();
        
        $i = Indicator::findOrFail($request->input('id'));
        $p = Project::findOrFail($i->id_proyecto);
     
       return view('beneficiarios.create',compact('tiposgestores', 'p') );
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        if (request('type') == 'indicador') 
            $this->validate($request, 
                [
                    'nombrebeneficiario' => 'required', 
                    'apellidobeneficiario' => 'required',
                    'genero'=> 'required',
                    'rangoedad'=> 'required',
                    'nombreubicacion'=> 'required',
                    'telefono' => 'required|numeric',
                    'emailbeneficiario'=> 'required'
                ]);
        else 
            $this->validate($request, 
                [
                    'nombrebeneficiario' => 'required', 
                    'apellidobeneficiario' => 'required',
                    'genero'=> 'required',
                    'rangoedad'=> 'required',
                    'nombreubicacion'=> 'required'
                ]);
        
        $beneficiarios = new Beneficiarios();
 
        if(request('type') == 'indicador'){
            $i = Indicator::findOrFail(request('indicator_id'));
            $p = Project::findOrFail($i->id_proyecto);
            $code_generate = substr($p->name,0, 7);
            $code_generate = strtoupper($code_generate);
            $code_generate = trim($code_generate);
            
            $lastId = Beneficiarios::latest()->first();
            $lastId = $lastId->id + 1;
            $digits = strlen($lastId);

            for ($i=$digits; $i < 7 ; $i++) 
                $lastId = '0' . $lastId;

            $code_generate = $code_generate . '-' . $lastId;

        } else {
            $a = Actividades::findOrFail(request('actividad_id'));
            $i = Indicator::findOrFail($a->indicator_id);
            $p = Project::findOrFail($i->id_proyecto);
            $code_generate = substr($p->name,0, 7);
            $code_generate = strtoupper($code_generate);
            $code_generate = trim($code_generate);
            
            $lastId = Beneficiarios::latest()->first();
            $lastId = $lastId->id + 1;
            $digits = strlen($lastId);

            for ($i=$digits; $i < 7 ; $i++) 
                $lastId = '0' . $lastId;

            $code_generate = $code_generate . '-' . $lastId;

        }

        $beneficiarios->code = $code_generate;
        $beneficiarios->nombrebeneficiario = request('nombrebeneficiario');
        $beneficiarios->apellidobeneficiario = request('apellidobeneficiario');
        $beneficiarios->genero = request('genero');
        $beneficiarios->rangoedad = request('rangoedad');
        $beneficiarios->nombreubicacion = request('nombreubicacion');
        if(request('type') == 'indicador'){
            $beneficiarios->estado = request('enable') == 'on' ? true:false;  
            $beneficiarios->dpicui = request('dpicui');
            $beneficiarios->telefono = request('telefono');
            $beneficiarios->emailbeneficiario = request('emailbeneficiario');
        }
        //Se deshabilito por cambios solicitador por RNGG 22/04/2020
        //$beneficiarios->tipobeneficiario = request('tipobeneficiario');

        if ( request('status') == 'on' )
            $beneficiarios->id_tipogestor=request('gestor');

        $beneficiarios->save();
 
        if(request('type') == 'indicador'){
            
            $beneficio = new Beneficio;
            $beneficio->indicator_id = request('indicator_id');
            $beneficio->beneficiario_id = $beneficiarios->id;
            $beneficio->save();
            
            $indicator_id = request('indicator_id');
            
            $indicador = Indicator::find($indicator_id);
            $indicador->accumulated = $indicador->accumulated + 1;

            $indicador->percentage = ($indicador->accumulated / $indicador->goal)*100;

            $indicador->update();
    
            return redirect("/beneficios/$indicator_id");

        } else {

            $participante= new Participantes;

            $participante->actividad_id=request('actividad_id');
            $participante->beneficiario_id = $beneficiarios->id;
            
            $participante->save();
            $actividad_id = request('actividad_id');
            return redirect("/participantes/$actividad_id");

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
  

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $beneficiarios = Beneficiarios::findOrFail($id);
        $tiposgestores= TipoGestor::all();

        $i = Indicator::findOrFail($request->input('id'));
        $p = Project::findOrFail($i->id_proyecto);
        
        return view('beneficiarios.edit',compact('beneficiarios', 'tiposgestores', 'p'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        
        $form_data = array('nombrebeneficiario' => $request->nombrebeneficiario,
                            'apellidobeneficiario' => $request->apellidobeneficiario,
                            'genero' => $request->genero,
                            'estado' => (( $request->estado == 'on') ? true:false ),
                            'rangoedad' => $request->rangoedad,
                            'nombreubicacion' => $request->nombreubicacion,
                            'dpicui' => $request->dpicui,
                            'telefono' => $request->telefono,
                            'emailbeneficiario' => $request->emailbeneficiario,
                            'id_tipogestor' => $request->gestor
                        );


        //$beneficiarios = Beneficiarios::findOrFail($id);

        Beneficiarios::whereId($id)->update($form_data);
        
        if(request('type') == 'indicador'){
            return redirect("/beneficios/$request->indicator_id")->with('success','Beneficiario actualizado correctamente!');

        } else {
            return redirect("/participantes/$request->actividad_id")->with('success','Beneficiario actualizado correctamente!');

        }
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $beneficiarios = Beneficiarios::findOrFail($id);
        $beneficiarios->delete();
       return redirect('beneficiarios')->with('success', 'Contact dELETE!');
    }
}

