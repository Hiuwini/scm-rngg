@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
   <div class="breadcrumb">
                <h1>Editar</h1>
                <ul>
                    <li>Beneficiarios</li>
                    <li><a href="">{{ $p->name }}</a></li>
                    
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Editar Beneficiario</div>

                            <form method="POST" action="{{ route( 'beneficiarios.update', $beneficiarios->id) }}" enctype="multipart/form-data">

                                
                                @csrf
                                @method('PATCH')
                                <div class="row">

                                @if($_GET['type'] == 'actividades')
                                    <input type="hidden" name="actividad_id" value="{{ $_GET['id'] }}">
                                    <input type="hidden" name="type" value="actividad">
                                @else
                                    <input type="hidden" name="indicator_id" value="{{ $_GET['id'] }}">
                                    <input type="hidden" name="type" value="indicador">
                                @endif
                                
                                   <div class="col-md-6 form-group mb-3">
                                    <label>Nombre</label>
                                    <div class="col-md-8">
                                        <input type="text" name="nombrebeneficiario" value="{{ $beneficiarios->nombrebeneficiario }}" class="no-special-char form-control input-lg" />
                                        <small id="passwordHelpBlock" class="ul-form__text form-text ">                                                
                                Ingrese el nombre del beneficiario.</small>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group mb-3">
                                    <label>Apellido</label>
                                    <div class="col-md-8">
                                        <input type="text" name="apellidobeneficiario" value="{{ $beneficiarios->apellidobeneficiario }}" class=" no-special-char form-control input-lg" />
                                        <small id="passwordHelpBlock" class="ul-form__text form-text ">                                                
                                Ingrese el apellido del beneficiario.</small>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group mb-3">
                                    <label>Género</label>
                                    <div class="col-md-8">
                                    <select name="genero" id="generoId" class="form-control" >
                                        <option value="">-- Seleccione el género --</option>
                                        <option value="Femenino" {{ ($beneficiarios->genero == 'Femenino' ) ? 'selected':'' }}>Femenino</option>    
                                        <option value="Masculino"{{ ($beneficiarios->genero == 'Masculino' ) ? 'selected':'' }}>Masculino</option>                                
                                    </select>
                                    </div>
                                </div>

                                
                                <div class="col-md-6 form-group mb-3">
                                    <label>Rango de Edad</label>
                                    <div class="col-md-8">
                                        <select name="rangoedad" id="rangoedadId" class="form-control" >
                                            <option value="">-- Seleccione el rango de edad--</option>
                                            <option value="Menor de 18" {{ ($beneficiarios->rangoedad == 'Menor de 18' ) ? 'selected':'' }}>Menor de 18</option>    
                                            <option value="18 - 30" {{ ($beneficiarios->rangoedad == '18 - 30' ) ? 'selected':'' }}>18 - 30</option>  
                                            <option value="31 - 49" {{ ($beneficiarios->rangoedad == '31 - 49' ) ? 'selected':'' }}>31 - 49</option>
                                            <option value="50 - 60" {{ ($beneficiarios->rangoedad == '50 - 60' ) ? 'selected':'' }}>50 - 60</option>                              
                                            <option value="Mayor de 60" {{ ($beneficiarios->rangoedad == 'Mayor de 60' ) ? 'selected':'' }}>Mayor de 60</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group mb-3">
                                    <label>Ubicación</label>
                                    <div class="col-md-8">
                                        <select name="nombreubicacion" id="nombreubicacionId" class="form-control" >
                                            <option value="">-- Seleccione la ubicación --</option>
                                            <option value="Urbana" {{ ($beneficiarios->nombreubicacion == 'Urbana' ) ? 'selected':'' }}>Urbana</option>
                                            <option value="Rural" {{ ($beneficiarios->nombreubicacion == 'Rural' ) ? 'selected':'' }}>Rural</option>
                                        </select>
                                    </div>
                                </div>

                            @if($_GET['type'] == 'indicator')
                                @if($p->flag_dpi)
                                <div class="col-md-6 form-group mb-3">
                                    <label>DPI / CUI</label>
                                    <div class="col-md-8">
                                        <input type="text" name="dpicui" value="{{ $beneficiarios->dpicui }}" class="no-special-char form-control input-lg" />
                                        <small id="passwordHelpBlock" class="ul-form__text form-text ">                                                
                                Ingrese el DPI/CUI del beneficiario.</small>
                                    </div>
                                </div>
                                @endif

                                  <div class="col-md-6 form-group mb-3">
                                    <label>Teléfono</label>
                                    <div class="col-md-8">
                                        <input type="number" name="telefono" value="{{ $beneficiarios->telefono }}" class="form-control input-lg" />
                                        <small id="passwordHelpBlock" class="ul-form__text form-text ">                                                
                                Ingrese el telefono del beneficiario.</small>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group mb-3">
                                    <label>Correo Electrónico</label>
                                    <div class="col-md-8">
                                        <input type="text" name="emailbeneficiario" value="{{ $beneficiarios->emailbeneficiario }}" class="form-control input-lg" />
                                        <small id="passwordHelpBlock" class="ul-form__text form-text ">                                                
                                Ingrese el correo electrónico del beneficiario.</small>
                                    </div>
                                </div>  

                                @if($p->flag_gg)
                                    @if(!$beneficiarios->id_tipogestor)
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="status">Miembro de Grupo Gestor</label><br>
                                        <label class="switch switch-primary mr-3">
                                                <span>No / Si</span>
                                                <input type="checkbox" name="status" id="status" >
                                                <span class="slider"></span>
                                        </label>
                                    </div>
                                    @endif
                                @endif

                                <div class="col-md-6 form-group mb-3">
                                    <div class="col-md-8">
                                        <label for="enable">Habilitar </label><br>
                                        <label class="switch switch-primary mr-3">
                                                <span>No / Si</span>
                                                <input type="checkbox" checked name="enable" id="enable" >
                                                <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
    
                                @if($p->flag_gg)
                                    @if($beneficiarios->id_tipogestor)
                                    <div class="col-md-6 form-group mb-3" id="typeGestorOnload">
                                        <div class="col-md-8">
                                            <label for="gestor">Miembro de: </label>
                                                <select name="gestor" class="form-control form-control-rounded">
                                                    <option value="">-- No pertenece a un Grupo Gestor --</option>
                                                    @foreach ($tiposgestores as $tipogestor)
                                                            <option value="{{ $tipogestor->id }}" {{ ($beneficiarios->id_tipogestor ==  $tipogestor->id ) ? 'selected':'' }}>{{ $tipogestor->nombre }} </option>
                                                    @endforeach
                                                </select>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-6 form-group mb-3" id="typeGestor">
                                        <div class="col-md-8">
                                            <label for="gestor">Miembro de: </label>
                                                <select name="gestor" class="form-control form-control-rounded">
                                                    <option value="">-- No pertenece a un Grupo Gestor --</option>
                                                    @foreach ($tiposgestores as $tipogestor)
                                                            <option value="{{ $tipogestor->id }}" {{ ($beneficiarios->id_tipogestor ==  $tipogestor->id ) ? 'selected':'' }}>{{ $tipogestor->nombre }} </option>
                                                    @endforeach
                                                </select>
                                        </div>
                                    </div>
                                    @endif
                                @endif
                            @endif
                                  <!-- Se deshabilita por requerimiento de RNGG
                                      <div class="col-md-6 form-group mb-3">
                                    <div class="col-md-8">
                                        <label>Sector del Beneficiario</label>
                                        <select name="tipobeneficiario" id="tipobeneficiarioId" class="form-control" >
                                            <option value="">-- Seleccione el sector del beneficiario --</option>
                                            <option value="Emprendedor" {{ ($beneficiarios->tipobeneficiario == 'Emprendedor' ) ? 'selected':'' }}>Sociedad Civil</option>
                                            <option value="Publico" {{ ($beneficiarios->tipobeneficiario == 'Publico' ) ? 'selected':'' }}>Publico</option>
                                            <option value="Privado" {{ ($beneficiarios->tipobeneficiario == 'Privado' ) ? 'selected':'' }}>Privado</option>                               
                                        </select>
                                        <small id="passwordHelpBlock" class="ul-form__text form-text ">                                                
                                        Seleccione el tipo del beneficiario.</small>
                                
                                    </div>-->
                                </div>
                                </div>

                                <div class="form-group text-left">
                                    <input type="submit" name="edit" class="btn btn-success m-1" value="Actualizar" />
                                    @if($_GET['type'] == 'actividades')
                                        <a style="margin: 19px;" href="/participantes/{{ $_GET['id'] }}" class="btn btn-danger m-1">Cancelar</a>
                                    @else
                                        <a style="margin: 19px;" href="/beneficios/{{ $_GET['id'] }}" class="btn btn-danger m-1">Cancelar</a>
                                    @endif
                                </div>
                             </form>  


                        </div>
                    </div>
                </div>

            </div>
        </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>

<script>
    $(document).ready(function() {
        $('.no-special-char').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9áéíóúÁÉÍÓÚÑñ' ]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
               event.preventDefault();
               return false;
            }
        });

    if ($('#typeGestor').length) {
        $('#typeGestor').hide();
        $('#status').change(function() {
        if($('#status').is(":checked")) {
                $('#typeGestor').show();
            }
            else {
                $('#typeGestor').hide();        
            }
        });   
    }

        
    });
</script>


@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
