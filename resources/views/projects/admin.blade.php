@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">

 <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <!-- <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables/dataTables.jqueryui.min.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables/buttons.jqueryui.min.css')}}"> -->

 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
 <style>
    body {
        table-layout: fixed;
      }
 </style>
@endsection

@section('main-content')
   <div class="breadcrumb">
                <h1>{{ $p[0]->name }}</h1>
                <ul>
                    <li>Proyecto</li>
                    <li><a href="">RNGG</a></li>
                    
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card mb-12">
                        <div class="card-body">
                            <div class="card-title mb-3">{{ $p[0]->name }}</div>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-basic-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-basic-tab" data-toggle="tab" href="#indicators" role="tab" aria-controls="indicators" aria-selected="false">Indicadores</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="contact-basic-tab" data-toggle="tab" href="#activity" role="tab" aria-controls="activity" aria-selected="false">Eventos</a>
                                    </li>
                                    <!--<li class="nav-item">
                                        <a class="nav-link" id="contact-basic-tab" data-toggle="tab" href="#goals" role="tab" aria-controls="goals" aria-selected="false">Metas</a>
                                    </li>-->
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="home-basic-tab">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">                                    
                                                    <h5>Nombre del proyecto</h5> <p>{{$p[0]->name}}</p>
                                                    <h5>Fecha de inicio</h5> <p>{{ date("d / m / Y", strtotime( $p[0]->date_begin ) ) }}</p>
                                                    <h5>Fecha de finalización</h5> <p>{{ date("d / m / Y", strtotime( $p[0]->date_end ) ) }}</p>
                                                   
                                                    <h5>Coodinador</h5> <p>{{$p[0]->firstname}} {{$p[0]->lastname}}</p>
                                                </div>

                                                <div class="col-md-6">
                                                <h5><b>{{$p[0]->name}}</b> - Progreso de proyecto  </h5>                                                    
                                                    
                                                <h6><b>Tiempo de ejecución</b>(en días) {{ $current }} transcurridos / {{ $diff }} totales</h6>
                                                    <div class="progress mb-3">
                                                        @if ($percentage < 51)
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" 
                                                            style="width: {{ $percentage }}%" aria-valuenow="{{ $current }}" aria-valuemin="0" 
                                                            aria-valuemax="{{$diff}}">{{  number_format($percentage , 0, '.', '') }}%
                                                        </div>
                                                        @elseif ($percentage > 50 && $percentage < 76 )
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" 
                                                            style="width: {{ $percentage }}%" aria-valuenow="{{ $current }}" aria-valuemin="0" 
                                                            aria-valuemax="{{$diff}}">{{  number_format($percentage , 0, '.', '') }}%
                                                        </div>
                                                        @else
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" 
                                                            style="width: {{ $percentage }}%" aria-valuenow="{{ $current }}" aria-valuemin="0" 
                                                            aria-valuemax="{{$diff}}">{{  number_format($percentage , 0, '.', '') }}%
                                                        </div>
                                                        @endif
                                                    </div>
                                                    
                                                    @foreach($indicators as $i)
                                                        <h6>{{$i->name}}</h6>
                                                        <div class="progress mb-3">
                                                            @if (((int)$i->percentage > 99))
                                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" 
                                                                style="width: {{ number_format((int)$i->percentage , 0, '.', '') }}%" aria-valuenow="{{ number_format((int)$i->percentage , 0, '.', '') }}" aria-valuemin="0" 
                                                                    aria-valuemax="100">{{ number_format((int)$i->percentage , 0, '.', '') }}%
                                                            </div>
                                                            @elseif (((int)$i->percentage > 50 && (int)$i->percentage < 100))
                                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" 
                                                                style="width: {{ number_format((int)$i->percentage , 0, '.', '') }}%" aria-valuenow="{{ number_format((int)$i->percentage , 0, '.', '') }}" aria-valuemin="0" 
                                                                    aria-valuemax="100">{{ number_format((int)$i->percentage , 0, '.', '') }}%
                                                            </div>
                                                            @elseif ((int)$i->percentage < 51)
                                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" 
                                                                style="width: {{ number_format((int)$i->percentage , 0, '.', '') }}%" aria-valuenow="{{ number_format((int)$i->percentage , 0, '.', '') }}" aria-valuemin="0" 
                                                                    aria-valuemax="100">{{ number_format((int)$i->percentage , 0, '.', '') }}%
                                                            </div>
                                                            @endif
                                                        </div>
                                                    @endforeach<h5><b>Descripción</b></h5>
                                                    
                                                    <p>{{$p[0]->description}}</p>                                                    
                                                    
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="indicators" role="tabpanel" aria-labelledby="profile-basic-tab">
                                        <!-- Aqui van el codigo de los indicadores -->    
                                        @if( (\App\Permiso::where('user_id','=',Auth::id())->get())[0]->rol_id <= 3)
                                        <a style="margin: 19px;" href="/indicators/create/{{ $p[0]->id }}" class="btn btn-primary">
                                            Nuevo Indicador
                                        </a>
                                        @endif
                                        <div class="row">
                                        <div class="table-responsive">
                                        <table id="principal" class="table table-striped table-hover dt-responsive display nowrap" >
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Indicador</th>
                                                        <th>Tipo</th>
                                                        <th>Fecha Limite</th>
                                                        <th>Fecha de Alerta</th>
                                                        <th width="40px">Avance</th>
                                                        <th width="40px">Meta</th>
                                                        <th>% de Avance</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($indicators as $i)
                                                    <tr>
                                                        <th scope="row"> {{ $loop->iteration }} </th>
                                                        <td>{{ $i->name }}</td>
                                                        @if ($i->type == 1)
                                                            <td> Asesorías individuales </td>
                                                        @elseif($i->type == 2)
                                                            <td> Eventos </td>
                                                        @else 
                                                            <td> Personalizado </td>
                                                        @endif
                                                        <td>{{ $i->date_limit }}</td>
                                                        <td style="background: {{ $i->date_alert < date("Y-m-d") ? '#fbff00':'#4472ab'}}">{{ $i->date_alert }}</td>
                                                        <td>{{ number_format((int)$i->accumulated , 0, '.', '') }}</td>
                                                        <td>{{ number_format((int)$i->goal , 0, '.', '') }}</td>
                                                        @if (((int)$i->percentage > 99))
                                                        <td style="background: #4caf50 !important">
                                                            {{ number_format((int)$i->percentage , 0, '.', '') }}%
                                                        </td>
                                                        @elseif (((int)$i->percentage > 50 && (int)$i->percentage < 100))
                                                        <td style="background: #ffc107 !important">
                                                            {{ number_format((int)$i->percentage , 0, '.', '') }}%
                                                        </td>
                                                        @elseif ((int)$i->percentage < 51 && (int)$i->percentage > 0)
                                                        <td style="background: #f44336 !important">
                                                            {{ number_format((int)$i->percentage , 0, '.', '') }}%
                                                        </td>
                                                        @else
                                                        <td>{{ number_format((int)$i->percentage , 0, '.', '') }}%</td>
                                                        @endif
                                                       <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn bg-white _r_btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <span class="_dot _inline-dot bg-primary"></span>
                                                                    <span class="_dot _inline-dot bg-primary"></span>
                                                                    <span class="_dot _inline-dot bg-primary"></span>
                                                                </button>
                                                                <div class="dropdown-menu" x-placement="bottom-start">
                                                                    @if ($i->type == 3)                                                        
                                                                        <a class="dropdown-item" href="/indicator/{{$i->id}}">Administrar</a>
                                                                    @elseif($i->type == 2)
                                                                        <a class="dropdown-item" href="/events/{{$i->id}}">Ver Eventos</a>
                                                                    @else
                                                                        <a class="dropdown-item" href="/beneficios/{{$i->id}}">Administrar</a>
                                                                    @endif
                                                                    @if( (\App\Permiso::where('user_id','=',Auth::id())->get())[0]->rol_id <= 3)
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item" href="{{ url("/indicators/$i->id/edit") }}">Actualizar</a>
                                                                    <form action="/indicators/{{$i->id}}" method="POST">                                                    
                                                                        @csrf
                                                                        @method('DELETE')   
                                                                        <input class="dropdown-item" type="submit" value="Eliminar" />
                                                                        
                                                                    </form>
                                                                    @endif
                                                                
                                                                </div>
                                                            </div>
                                                           
                                                        </td>
                                                    </tr>
                                                    @endforeach                    
                                                </tbody>
                                            </table>  
                                        </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane fade" id="activity" role="tabpanel" aria-labelledby="contact-basic-tab">
                                        <!-- Aqui van el codigo de las actividades -->    
                                        <!--<a style="margin: 19px;" href="{{ route('actividades.create', $p[0]->id)}}" class="btn btn-primary">Nueva Actividad</a>-->
                                        <div class="row">

                                     <table class="table" id="events">
                                         <thead class="thead-light">
                                             <tr>
                                                 <th>No.</th>
                                                 <th>Nombre</th>
                                                 <th>Descripción</th>
                                                 <th>Fecha</th>
                                                 <th>Cantidad proyectada</th>
                                                 <th>Duración en horas</th>
                                                 <th>Proyecto</th>
                                             </tr>
                                         </thead>
                                         <tbody>
                                            
                                        @foreach($actividades as $actividad)
                                        <tr>
                                            <th scope="row"> {{ $loop->iteration }} </th>
                                            <td>{{ $actividad->nombre }}</td>
                                            <td>{{ $actividad->descripcion}} </td>
                                            <td>{{ $actividad->fecha }}</td>
                                            <td>{{ $actividad->cantidadProyectada }}</td>
                                            <td>{{ $actividad->duracion }}</td>
                                            
                            
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn bg-white _r_btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="_dot _inline-dot bg-primary"></span>
                                                        <span class="_dot _inline-dot bg-primary"></span>
                                                        <span class="_dot _inline-dot bg-primary"></span>
                                                    </button>
                                                    <div class="dropdown-menu" x-placement="bottom-start">
                                                        
                                                            <a class="dropdown-item" href="/participantes/{{$actividad->id}}">Administrar</a>
                                                        
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item" href="{{ url("/indicators/$i->id/edit") }}">Actualizar</a>
                                                        <form action="/indicators/{{$i->id}}" method="POST">                                                    
                                                            @csrf
                                                            @method('DELETE')   
                                                            <input class="dropdown-item" type="submit" value="Eliminar" />
                                                            
                                                        </form>
                                                    
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                        @endforeach                    
                                         </tbody>
                                        
                                     </table>  
                                </div>
                                    </div>
                                    
                                </div>
                        </div>
                    </div>
                </div>
                
                
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

<script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.jqueryui.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.jqueryui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>

<!-- Aqui se edita la grafica -->
<script src="{{asset('assets/js/es5/chartjs.script.min.js')}}"></script>
<!-- Aqui se edita la grafica -->
<script>
$(document).ready(function() {
  
     $('#principal thead tr').clone(true).appendTo( '#principal thead' );
     $('#principal thead tr:eq(1) th').each( function (i) {
         var title = $(this).text();

        switch(title){
            case 'Acciones': $(this).html( '' ); break;
            case 'No.': $(this).html( '' ); break;
            default: $(this).html( '<input type="text" style="width:100%" placeholder="'+title+'" />' );
        }

         $( 'input', this ).on( 'keyup change', function () {
             if ( table.column(i).search() !== this.value ) {
                 table
                     .column(i)
                     .search( this.value )
                     .draw();
             }
         } );
     } );
  
     var table = $('#principal').DataTable( {
         buttons: [ 'copy', 'excel', 'pdf'],
         autoWidth: true
     } );

     table.buttons().container()
     .insertAfter( '#principal' );
     table.columns.adjust().draw();

     $('#events thead tr').clone(true).appendTo( '#events thead' );
     $('#events thead tr:eq(1) th').each( function (i) {
         var title = $(this).text();

        switch(title){
            case 'Acciones': $(this).html( '' ); break;
            case 'No.': $(this).html( '' ); break;
            default: $(this).html( '<input type="text" style="width:100%" placeholder="Buscar '+title+'" />' );
        }

         $( 'input', this ).on( 'keyup change', function () {
             if ( table.column(i).search() !== this.value ) {
                 table
                     .column(i)
                     .search( this.value )
                     .draw();
             }
         } );
     } );
  
     var table = $('#events').DataTable( {
         buttons: [ 'copy', 'excel', 'pdf'],
     } );

     table.buttons().container()
     .insertAfter( '#events' );
     
    });
</script>
@endsection

@section('bottom-js')


@endsection