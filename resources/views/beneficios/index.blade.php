@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">

 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.dataTables.min.css">

 <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.jqueryui.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.jqueryui.min.css">

 <style>thead input {
    width: 100%;
}</style>


@endsection

@section('main-content')
   <div class="breadcrumb">
                <h1>Beneficiados</h1>
                <ul>
                    
                    <li><a href="">{{$project->name}}</a></li>
                    
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Listado de Beneficiarios
                                <a style="margin: 19px;" href="{{ url("/beneficiarios/create/?id=$i->id&type=indicador") }}" class="btn btn-primary">Nuevo Beneficiario</a>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".beneficiary">Buscar Beneficiario</button>
                            </div>
                            
                                <div class="row">
                                <p>Los beneficiarios contribuyen al siguiente indicador: <b>{{$i->name}}</b></p>
                                     <table class="display table table-striped table-bordered" id="principal">
                                         <thead class="thead-light">
                                             <tr>
                                                 <th>No</th>
                                                 <th>Código</th>
                                                 <th>Nombre</th>
                                                 <th>Apellido</th>
                                                 <th>Género</th>
                                                 <th>Rango de Edad</th>
                                                 <th>Ubicación</th>
                                                 <th>Operaciones</th>
                                                 
                                             </tr>
                                         </thead>
                                         <tbody>
                                             @foreach($beneficiarios as $beneficario)
                                             <tr>
                                                 <td>{{$loop->iteration}}</td>
                                                 <td>{{ $beneficario->code}}</td>
                                                 <td>{{ $beneficario->nombrebeneficiario}}</td>
                                                 <td>{{ $beneficario->apellidobeneficiario}}</td>
                                                 <td>{{ $beneficario->genero}}</td>
                                                 <td>{{ $beneficario->rangoedad}}</td>
                                                 <td>{{ $beneficario->nombreubicacion}}</td>
                                                 <td>
                                                    <div class="row">
                                                        <div class="col-md-6"><a href="{{ route('beneficiarios.edit', [$beneficario->beneficiario_id,  'type'=>'indicator', 'id'=>$i->id])}}" class="btn btn-warning">Editar</a></div>
                                                        <div class="col-md-6">
                                                            <form action="/beneficios/{{$beneficario->id}}/{{$i->id}}" method="POST">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                                             
                                                            </form>
                                                        </div>
                                                    </div> 
                                                </td>
                                             </tr>
                                             @endforeach
                                            
                                         </tbody>
                                        
                                     </table>  
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
            <!-- Large Modal -->
            <div class="modal fade beneficiary" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Seleccionar beneficiarios</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <a href="{{ url("/beneficiarios/create/?id=$i->id&type=indicador") }}" class="btn btn-primary">Nuevo Beneficiario</a>
                            </div>
                        </div>
                        <div class="modal-body">
                            
                            <table id="example" class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>DPI/CUI</th>
                                        <th>Teléfono</th>
                                        <th>Email</th>
                                        <th>Agregar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($all as $beneficiary)
                                    <tr>
                                        <td>{{ $beneficiary->nombrebeneficiario }}</td>
                                        <td>{{ $beneficiary->apellidobeneficiario }}</td>
                                        <td>{{ $beneficiary->dpicui }}</td>
                                        <td>{{ $beneficiary->telefono }}</td>
                                        <td>{{ $beneficiary->emailbeneficiario }}</td>
                                        <td>
                                            <label class="checkbox checkbox-primary">
                                                <input type="checkbox" class="adding" value="{{$beneficiary->id}}">
                                                <span> Agregar </span>
                                                <span class="checkmark"></span>
                                            </label>
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>DPI/CUI</th>
                                        <th>Teléfono</th>
                                        <th>Email</th>
                                        <th>Agregar</th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-primary" id="send_items">Agregar a indicador</button>
                        </div>
                    </div>
                </div>
            </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>

<script src="https://cdn.datatables.net/1.10.20/js/dataTables.jqueryui.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.jqueryui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>

<script>
$(document).ready(function() {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
     // Setup - add a text input to each footer cell - for principal
     $('#principal thead tr').clone(true).appendTo( '#principal thead' );
     $('#principal thead tr:eq(1) th').each( function (i) {
         var title = $(this).text();
         if (title != 'Operaciones')
            $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
        else
            $(this).html( '' );

         $( 'input', this ).on( 'keyup change', function () {
             if ( table.column(i).search() !== this.value ) {
                 table
                     .column(i)
                     .search( this.value )
                     .draw();
             }
         } );
     } );
  
     var table = $('#principal').DataTable( {
         buttons: [ 'copy', 'excel', 'pdf', 'colvis'],
     } );

     table.buttons().container()
     .insertBefore( '#principal' );


    // Setup - add a text input to each footer cell
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        if (title != 'Agregar')
            $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
        else
            $(this).html( '' );
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
 
    var table2 = $('#example').DataTable( {
        orderCellsTop: true,
        fixedHeader: true
    } );
    $('#send_items').click(function(){
        //alert();
        //console.log($("input.adding:checkbox"));
        var inputs = $("input.adding:checked");
        var ids = [];
        ids.push({{$i->id}});
        $.each(inputs,function(i,val){
            ids.push(val.value);
        });
        
        $.ajax({
                url: "/beneficios/store/"+JSON.stringify(ids),
                type: "GET",
                success: function(data) {
                    location.reload();
                },
                error: function(data){
                    console.log(data);
                }
            });
    });
} );
</script>

@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
