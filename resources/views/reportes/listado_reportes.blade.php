@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">

 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.dataTables.min.css">

 <style>thead input {
    width: 100%;
}</style>


@endsection

@section('main-content')
                    
<div class="breadcrumb">
                <h1>REPORTES</h1>
                
            </div>

            <div class="separator-breadcrumb border-top"></div>
            <div class="2-columns-form-layout">
                <div class="">
                    <div class="row">
                        <div class="col-lg-12">                        
                         
                                <!--begin::form-->

                                    <div class="card-body">



                         
                                <!-- start card 3 Columns Horizontal Form Layout-->
                                <div class="card ul-card__margin-25">
                                    <div class="card-header bg-transparent">
                                        <h3 class="card-title"> Reporte de Beneficiarios</h3>
                                        <p>Se visualizan la cantidad de {{$totalbeneficiarios}} beneficiarios, {{$cantM}} mujeres y {{$cantH}} hombres.</p>
                                    </div>
                                <!-- Grafica de Pie para Generos-->
                                    <div class="card mb-4">
                                                    <div class="card-body">
                                                        <div class="card-title">Grafica de Generos</div>  
                                                        <div id="echartPieGenero" style="height: 300px; width: 500px;"></div>
                                                    </div>
                                    </div>
                                            

                                    <div class="card-body">
                                        <div class="form-group row">
                            <table id="example" class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Rango Edad</th>
                                        <th>Evento / Indicador</th>
                                        <th>Proyecto</th>
                                        <th>Correo </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($beneficiarios as $beneficiary)
                                    <tr>
                                        <td>{{ $beneficiary->nombrebeneficiario }}</td>
                                        <td>{{ $beneficiary->apellidobeneficiario }}</td>
                                        <td>{{ $beneficiary->rangoedad }}</td>
                                        <td>{{ $beneficiary->participacion }}</td>
                                        <td>{{ $beneficiary->proyecto }}</td>
                                        <td>{{ $beneficiary->emailbeneficiario }}</td>
                                        
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                          <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Rango Edad</th>
                                        <th>Nombre Indicador</th>
                                        <th>Proyecto</th>
                                        <th>Correo </th>
                                    </tr>
                                </tfoot>
                            </table>
                            
                            
                    
               


                                          

                                            
                         </div>



                                        <div class="custom-separator"></div>

                                        </div>



                                    </div>
                                    <div class="card-footer">
                                        <div class="mc-footer">
                                            <div class="row text-center">
                                                <div class="col-lg-12 ">
                                               
                                               <a href=""> <button class="btn btn-primary">Generar</button></a>
                                                
                                               

                                            
                                                   <a href="home"> <button type="button" class="btn btn-outline-secondary m-1">Cancelar</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end card 3 Columns Horizontal Form Layout-->
                            
                            <!-- end::form 3-->





                        </div>

                    </div>
                    <!-- end of main row -->
                </div>
            </div>
                                        
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>
<script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
<script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>

<script>
$(document).ready(function() {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    // Setup - add a text input to each footer cell
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
 
    var table = $('#example').DataTable( {
        orderCellsTop: true,
        fixedHeader: true
    } );
    
        
      
    });

</script>


<script >
     //PARA GRAFICAR

'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
var cantF,cantM,maxGenero,deCuanto,uno,dos,tres,cuatro,cinco,maxedad,cuantoedad;
//-------------------------------------------variables para genero
cantF={{$cantM}};
cantM={{$cantH}};
maxGenero={{$maxGenero}};
deCuanto=maxGenero/4;
//------------------------------------------variables para rango de edad
uno={{$rangoUno}};
dos={{$rangoDos}};
tres={{$rangoTres}};
cuatro={{$rangoCuatro}};
cinco={{$rangoCinco}};
maxedad={{$maximo}};
cuantoedad=maxedad/4;

$(document).ready(function () {
//------------------------------------------------inicio genero
    //PARA GRAFICAS PIE
    var echartElemPie = document.getElementById('echartPieGenero');
    if (echartElemPie) {
    var echartPie = echarts.init(echartElemPie);
    echartPie.setOption({
        color: ['#62549c', '#7566b5', '#7d6cbb', '#8877bd', '#9181bd', '#6957af'],
        tooltip: {
            show: true,
            backgroundColor: 'rgba(0, 0, 0, .8)'
        },
        series: [{
            name: 'Genero',//descripción flotante de los valores
            type: 'pie',
            radius: '50%',
            center: ['50%', '50%'],
            //ingresar los valores y etiquetas para cada lado
            
            data: [{ value: cantM, name: 'Masculino ' }, 
            { value: cantF, name: 'Femenino' }],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }]
    });
    $(window).on('resize', function () {
        setTimeout(function () {
            echartPie.resize();
        }, 2000);
    });
}



});
     </script>

@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
