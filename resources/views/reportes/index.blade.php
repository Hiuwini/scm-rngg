@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')

                            <!--begin::form 2-->
                            <form action="">
                                <!-- start card 2 Columns Horizontal Form Layout-->
                                <div class="card ul-card__margin-25">
                                    <div class="card-header bg-transparent">
                                        <h3 class="card-title"> Generar reporte personalizado</h3>
                                    </div>

                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label for="staticEmail6" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">
                                                Proyecto:</label>
                                            <div class="col-lg-2">
                                                <select name="projectid" id="projectid" class="form-control">
                                                <option value="">Seleccione un proyecto</option>
                                                </select>
                                                <small id="passwordHelpBlock" class="ul-form__text form-text ">
                                                    Seleccione el proyecto que desea filtrar.
                                                </small>
                                            </div>

                                            <label for="staticEmail7" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">Contact
                                                Number:</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" id="staticEmail7" placeholder="Enter Contact Number">
                                                <small id="passwordHelpBlock" class="ul-form__text ">
                                                    Please enter your Contact Number
                                                </small>
                                            </div>
                                        </div>



                                        <div class="custom-separator"></div>



                                        <div class="form-group row">
                                            <label for="staticEmail8" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">Address:</label>
                                            <div class="col-lg-2">

                                                <div class="input-right-icon">
                                                    <input type="text" class="form-control" id="inputEmail8" placeholder="Enter your address">
                                                    <span class="span-right-input-icon">
                                                        <i class="ul-form__icon i-Map-Marker"></i>
                                                    </span>
                                                </div>

                                                <small id="passwordHelpBlock" class="ul-form__text form-text ">
                                                    Please enter your address
                                                </small>
                                            </div>

                                            <label for="staticEmail9" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">Postcode:</label>
                                            <div class="col-lg-2">
                                                <div class="input-right-icon">
                                                    <input type="text" class="form-control" id="inputEmail9" placeholder="Enter your postcode">
                                                    <span class="span-right-input-icon">
                                                        <i class="ul-form__icon i-New-Mail"></i>
                                                    </span>
                                                </div>

                                                <small id="passwordHelpBlock" class="ul-form__text form-text ">
                                                    Please enter your postcode
                                                </small>
                                            </div>
                                        </div>

                                        <div class="custom-separator"></div>
                                        <div class="form-group row">
                                            <label for="staticEmail10" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">Group:</label>
                                            <div class="col-lg-2">

                                                <div class="ul-form__radio-inline">
                                                    <label class=" ul-radio__position radio radio-primary form-check-inline">
                                                        <input type="radio" name="radio" value="0">
                                                        <span class="ul-form__radio-font">Sales Person</span>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="ul-radio__position radio radio-primary">
                                                        <input type="radio" name="radio" value="0">
                                                        <span class="ul-form__radio-font">Customer</span>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>

                                                <small id="passwordHelpBlock" class="ul-form__text form-text ">
                                                    Please enter your address
                                                </small>
                                            </div>



                                        </div>



                                    </div>
                                    <div class="card-footer">
                                        <div class="mc-footer">
                                            <div class="row text-right">
                                                <div class="col-lg-4 ">
                                                    <button type="button" class="btn btn-primary m-1">Generar</button>
                                                    <button type="button" class="btn btn-outline-secondary m-1">Salir</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end card 2 Columns Horizontal Form Layout-->
                            </form>
                            <!-- end::form 2-->

@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
