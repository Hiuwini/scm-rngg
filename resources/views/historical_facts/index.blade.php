@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">

 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
@endsection

@section('main-content')
   <div class="breadcrumb">
                <h1>Datos Historicos</h1>
                <ul>
                    <li>RNGG</li>
                    <li><a href="">Administración</a></li>
                    
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                
                <div class="col-lg-6 col-xl-6 mt-3">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Configuración de datos historicos</div>
                        <form class="needs-validation" novalidate method="POST" action="{{ url('/set_historical_facts') }}">
                            {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="qt_beneficiaries">Beneficiarios acumulados</label>
                                        <input type="number" class="form-control form-control-rounded" name="qt_beneficiaries" 
                                        id="qt_beneficiaries" placeholder="Total de beneficiarios acumulados fuera del sistema" required>
                                    </div>
                                    
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="date_of_verification">Ultima fecha de verificación</label>
                                        <div class="input-group">
                                            <input id="date_of_verification" name="date_of_verification" class="form-control form-control-rounded" placeholder="yyyy-mm-dd" required>
                                            <div class="input-group-append">
                                                <button class="btn btn-secondary btn-rounded"  type="button">
                                                    <i class="icon-regular i-Calendar-4"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                         <button class="btn btn-success" type="submit">Volver a Generar Datos Historicos</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">Datos Historicos</div>
                            <div class="ul-widget-s6__items ul-widget-card__position">
                                <div class="ul-widget-card__item">
                                    <span class="ul-widget-s6__badge ul-widget-card__dot">
                                        <p class="badge-dot-primary ul-widget6__dot ul-widget-card__dot-xl">
                                            <i class="i-Add text-white"></i>
                                        </p>
                                    </span>
                                    <div class="ul-widget-card__info-v2">
                                        <span class="t-font-boldest">Total de Beneficiarios Acumulados</span>
                                        <span class="t-font-bold">{{ number_format((int)$hf->qt_beneficiaries , 0, '.', ',') }}</span>
                                        <small class="text-mute">RNGG</small>
                                    </div>
                                </div>

                            </div>
                            <div class="ul-widget-s6__items ul-widget-card__position">
                                <div class="ul-widget-card__item">
                                    <span class="ul-widget-s6__badge ul-widget-card__dot">
                                        <p class="badge-dot-success ul-widget6__dot ul-widget-card__dot-xl">
                                            <i class="i-Like-2 text-white"></i>
                                        </p>
                                    </span>
                                    <div class="ul-widget-card__info-v2">
                                        <span class="t-font-boldest">Ultima Fecha de Verificación </span>
                                        <span class="t-font-bold">{{ $hf->date_of_verification }}</span>
                                        <small class="text-mute">RNGG</small>
                                    </div>
                                </div>
                            </div>
                            <div class="ul-widget-s6__items ul-widget-card__position">
                                <div class="ul-widget-card__item">
                                    <span class="ul-widget-s6__badge ul-widget-card__dot">
                                        <p class="badge-dot-primary ul-widget6__dot ul-widget-card__dot-xl">
                                            <i class="i-Check text-white"></i>
                                        </p>
                                    </span>
                                    <div class="ul-widget-card__info-v2">
                                        <span class="t-font-boldest">Ultima Fecha de Generación </span>
                                        <span class="t-font-bold">{{ $hf->date_of_generation }}</span>
                                        <small class="text-mute">RNGG</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-6 mt-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">Datos Historicos en Ejes Estrategicos</div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="ul-widget__row">
                                                <div class="ul-widget-stat__font">
                                                    <i class="i-Building text-primary"></i>
                                                </div>
                                                <div class="ul-widget__content">
                                                    <p class=" m-0">
                                                        Desarrollo Productivo
                                                    </p>
                                                    <h4 class="heading">{{ number_format((int)$hf->estimate_eje_1 , 0, '.', ',') }}</h4>
                                                    <input type="hidden" id="g_eje_1" value="{{ $hf->estimate_eje_1 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="ul-widget__row">
                                                <div class="ul-widget-stat__font">
                                                    <i class="i-Cloud-Sun text-primary"></i>
                                                </div>
                                                <div class="ul-widget__content">
                                                    <p class="m-0">
                                                        Clima de Negocios 
                                                    </p>
                                                    <h4 class="heading">{{ number_format((int)$hf->estimate_eje_2 , 0, '.', ',') }}</h4>
                                                    <input type="hidden" id="g_eje_2" value="{{ $hf->estimate_eje_2 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="ul-widget__row">
                                                <div class="ul-widget-stat__font">
                                                    <i class="i-Network text-primary"></i>
                                                </div>
                                                <div class="ul-widget__content">
                                                    <p class=" m-0">
                                                        Desarrollo Social
                                                    </p>
                                                    <h4 class="heading">{{ number_format((int)$hf->estimate_eje_3 , 0, '.', ',') }}</h4>
                                                    <input type="hidden" id="g_eje_3" value="{{ $hf->estimate_eje_3 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="card-title">Gráfica</div>  
                                    <div id="echartPieEjes" style="height: 300px; width: 500px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-4 mt-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">Datos Historicos en Género</div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="card mb-2">
                                            <div class="card-body">
                                                <div class="ul-widget__row">
                                                    <div class="ul-widget-stat__font">
                                                        <i class="i-Female text-primary"></i>
                                                    </div>
                                                    <div class="ul-widget__content">
                                                        <p class=" m-0">
                                                            Mujeres
                                                        </p>
                                                        <h4 class="heading">{{ number_format((int)$hf->estimate_female , 0, '.', ',') }}</h4>
                                                        <input type="hidden" id="g_female" value="{{ $hf->estimate_female }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="card mb-2">
                                            <div class="card-body">
                                                <div class="ul-widget__row">
                                                    <div class="ul-widget-stat__font">
                                                        <i class="i-Male text-primary"></i>
                                                    </div>
                                                    <div class="ul-widget__content">
                                                        <p class=" m-0">
                                                            Hombres
                                                        </p>
                                                        <h4 class="heading">{{ number_format((int)$hf->estimate_male , 0, '.', ',') }}</h4>
                                                        <input type="hidden" id="g_male" value="{{ $hf->estimate_male }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-4">
                                    <div class="card-body">
                                        <div class="card-title">Gráfica</div>  
                                        <div id="echartPieGenero" style="height: 300px; width: 500px;"></div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xl-8 mt-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">Datos Historicos en Rango de Edades</div>
                            
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="ul-widget__row">
                                                <div class="ul-widget-stat__font">
                                                    <i class="i-Folder-Archive text-primary"></i>
                                                </div>
                                                <div class="ul-widget__content">
                                                    <p class=" m-0">
                                                        Menores de 18 años
                                                    </p>
                                                    <h4 class="heading">{{ number_format((int)$hf->estimate_range_age_1 , 0, '.', ',') }}</h4>
                                                    <input type="hidden" id="g_range_age_1" value="{{ $hf->estimate_range_age_1 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="ul-widget__row">
                                                <div class="ul-widget-stat__font">
                                                    <i class="i-Folder-Archive text-primary"></i>
                                                </div>
                                                <div class="ul-widget__content">
                                                    <p class=" m-0">
                                                        18-30 años
                                                    </p>
                                                    <h4 class="heading">{{ number_format((int)$hf->estimate_range_age_2 , 0, '.', ',') }}</h4>
                                                    <input type="hidden" id="g_range_age_2" value="{{ $hf->estimate_range_age_2 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="ul-widget__row">
                                                <div class="ul-widget-stat__font">
                                                    <i class="i-Folder-Archive text-primary"></i>
                                                </div>
                                                <div class="ul-widget__content">
                                                    <p class=" m-0">
                                                        31-49 años
                                                    </p>
                                                    <h4 class="heading">{{ number_format((int)$hf->estimate_range_age_3 , 0, '.', ',') }}</h4>
                                                    <input type="hidden" id="g_range_age_3" value="{{ $hf->estimate_range_age_3 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="ul-widget__row">
                                                <div class="ul-widget-stat__font">
                                                    <i class="i-Folder-Archive text-primary"></i>
                                                </div>
                                                <div class="ul-widget__content">
                                                    <p class=" m-0">
                                                        50 - 60 años
                                                    </p>
                                                    <h4 class="heading">{{ number_format((int)$hf->estimate_range_age_4 , 0, '.', ',') }}</h4>
                                                    <input type="hidden" id="g_range_age_4" value="{{ $hf->estimate_range_age_4 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="ul-widget__row">
                                                <div class="ul-widget-stat__font">
                                                    <i class="i-Folder-Archive text-primary"></i>
                                                </div>
                                                <div class="ul-widget__content">
                                                    <p class=" m-0">
                                                        Mayores de 60 años
                                                    </p>
                                                    <h4 class="heading">{{ number_format((int)$hf->estimate_range_age_5 , 0, '.', ',') }}</h4>
                                                    <input type="hidden" id="g_range_age_5" value="{{ $hf->estimate_range_age_5 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="card-title">Gráfica</div>  
                                    <div id="echartPieEdades" style="height: 300px; width: 500px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
                
                
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
<script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>
<script src="{{asset('assets/js/historicalFactsCharts.js')}}"></script>
@endsection

@section('bottom-js')

<script>
    $(document).ready(function(){
        $('#date_of_verification').pickadate({
            format: 'yyyy-mm-dd',
            closeOnSelect: true,
            closeOnClear: true,
        });
    });
</script>

<script src="{{asset('assets/js/form.validation.script.js')}}"></script>

@endsection