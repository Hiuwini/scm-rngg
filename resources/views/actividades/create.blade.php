@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
   <div class="breadcrumb">
                <h1>Nuevo</h1>
                <ul>
                    <li>Evento</li>
                    <li><a href="">Proyecto {{ $project->name }}</a></li>
                    
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
             

                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Crear Nuevo Evento</div>
                            <p><b>Indicador:</b> {{ $indicator->name}}</p>
                            
                            <form method="POST" action="/actividades" >
                                {{ csrf_field() }}

                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach                                     
                                    </ul>                                  
                                </div> 
                                @endif

                        <div class="row">

                            <div class="col-md-6 form-group mb-3">
                                <label>Nombre</label>
                                <input type="text" name="nombre" class="form-control input-lg" value="{{ old('nombre') }}" />
                                <small id="passwordHelpBlock" class="ul-form__text form-text ">                                                
                                Ingrese el nombre del evento.</small>
                            </div>

                             <div class="col-md-6 form-group mb-3">
                             <label>Descripción</label>
                                <input type="text" name="descripcion" class="form-control input-lg" value="{{ old('descripcion') }}"/>
                                <small id="passwordHelpBlock" class="ul-form__text form-text ">                                                
                                Ingrese la descripción del evento</small>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="date_begin">Fecha de inicio</label>
                                <div class="input-group">
                                    <input id="fecha" name="fecha" class="form-control form-control-rounded" placeholder="yyyy-mm-dd">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary btn-rounded"  type="button">
                                            <i class="icon-regular i-Calendar-4"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                             <label>Cantidad de participantes proyectados</label>
                                <input type="number" name="cantidadProyectada" class="form-control input-lg" value="{{ old('cantidadProyectada') }}"/>
                                <small id="passwordHelpBlock" class="ul-form__text form-text ">                                                
                                Ingrese la cantidad de participantes proyectados</small>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                             <label>Duración en horas</label>
                                <input type="number" name="duracion" class="form-control input-lg" value="{{ old('duracion') }}"/>
                                <small id="passwordHelpBlock" class="ul-form__text form-text ">                                                
                                Ingrese la duracion del evento en horas</small>
                            </div>
                            <input type="hidden" name="id_proyecto" value="{{$project->id}}">
                            <input type="hidden" name="indicator_id" value="{{$indicator->id}}">
                            
                            </div>     

                             <div class="form-group text-left">

                                <button type="submit" class="btn btn-success m-1">Crear</button>
 
                                        <a style="margin: 19px;" href="/events/{{$indicator->id}}" class="btn btn-danger m-1">Cancelar</a>
                                    </div> 
                                    </form>   
             </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>

<script>
    $('#fecha').pickadate({
        format: 'yyyy-mm-dd',
        closeOnSelect: true,
        closeOnClear: true,
    });
</script>

@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
